


#include "optoforce.hpp"




Optoforce :: Optoforce(){
    offX=0;
    offY=0;
    offZ=0;
}

void
Optoforce :: init(const char *comport, int ID){
	int bdrate=115200;

	cport_nr=ID;
	if(RS232_OpenComport(comport, cport_nr, bdrate))
	{
		printf("Can not open comport\n");

		return;
	}
	update();
	offX=Fx;
	offY=Fy;
	offZ=Fz;
}

int
Optoforce :: update(){
		int j;
		n = RS232_PollComport(cport_nr, buf, 4095);
		//std::cout << n << std::endl;
		if(n >= 29){
			buf[n] = 0;   // always put a "null" at the end of a string! 

			j=n/29;
			j=(j-1)*29;
			if(buf[j]!=55 || buf[j+1]!=68){
				return -1;
			}

			conf=buf[j+2];
			Fx=buf[j+3]; Fx=Fx<<8; Fx=Fx|buf[j+4];
			Fy=buf[j+5]; Fy=Fy<<8; Fy=Fy|buf[j+6];
			Fz=buf[j+7]; Fz=Fz<<8; Fz=Fz|buf[j+8];
			T=buf[j+9]; T=T<<8; T=T|buf[j+10];
			
			for(int k=0; k<4; k++){
				S[k] = buf[j+11+k*2]; S[k]=S[k]<<8; S[k]=S[k]|buf[j+12+k*2];
				St[k] = buf[j+19+k*2]; St[k]=St[k]<<8; St[k]=St[k]|buf[j+20+k*2];
			}
			

			FxC=Fx-offX;
			FyC=Fy-offY;
			FzC=Fz-offZ;
			// checksum
			static short int chcksum, chcksum_rec;
			chcksum=0;
			chcksum_rec=buf[j+27]; chcksum_rec=chcksum_rec<<8; chcksum_rec=chcksum_rec|buf[j+28];
			for(int k=2; k<=26;k++){
			    chcksum+=buf[j+1*k];
			}
			if(chcksum!=chcksum_rec){
				std::cout << "wrong checksum: " << chcksum << "\t" << chcksum_rec << std::endl;
			    return -1;
			}
		}	
		else 
		    return -1;
		return 1;
		
}

void 
Optoforce :: setConfig(char conf){

	RS232_SendByte(cport_nr, conf);

}

void
Optoforce :: print(){
	std::cout << "ID: " << cport_nr << "\t" << "FX: " << Fx << "\t\t" << "FY: " << Fy << "\t\t" << "FZ: " << Fz << "\t\t" << "T: " << T << std::endl;
}

void
Optoforce :: printWithRemovedOffset(){
	std::cout << "ID: " << cport_nr << "\t" << "FX: " <<  FxC << "\t\t" << "FY: " << FyC << "\t\t" << "FZ: " << FzC << "\t\t" << "T: " << T << std::endl;
}
