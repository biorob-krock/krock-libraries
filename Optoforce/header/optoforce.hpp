#include "rs232.h"
#include <iostream>
#include <iomanip>
#ifndef OPTOFORCE_HPP
#define OPTOFORCE_HPP

class Optoforce{

	public:
		//================== public variables ===============================================
		short int Fx, Fy, offX, offY, FxC, FyC, FzC;
		unsigned short int Fz, T, offZ, S[4], St[4];
		char conf, chksum;

		//================== public functions ===============================================
		Optoforce();
		void init(const char *comport, int ID);
		void setConfig(char conf);
		int update();
		void print();
		void printWithRemovedOffset();
		
		

	private:
		//================== private variables ===============================================
		int n, cport_nr;
		unsigned char buf[4096];


		//================== private functions ===============================================


};


#endif

