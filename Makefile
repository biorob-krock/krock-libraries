LIB_HOME = /home/biorob/libkrock2
INCLUDE_DIRS = -I$(LIB_HOME)/DXLite-master -I$(LIB_HOME)/DXLite-master/dxsystem \
				-I$(LIB_HOME)/Comm -I$(LIB_HOME)/Comm/header \
				-I$(LIB_HOME)/vectornav/header \
				-I$(LIB_HOME)/Optoforce/header \
				-I$(LIB_HOME)/nnTools \
				-I$(LIB_HOME)

CXX = g++
CXXFLAGS += -fPIC -std=c++11	-O3  -DLINUX  -Wall -fmessage-length=0 -fPIC $(INCLUDE_DIRS)
cflags=-fPIC
#LIBS += -lpthread -lrt -ljpeg -lserial -lboost_system -lboost_thread -shared
LIBS += -lpthread -lrt -lboost_system -lboost_thread -shared
LDFLAGS = -shared 
RM = rm -f
TARGET_LIB1 = libdxlite.so
TARGET_LIB2 = libps3comm.so
TARGET_LIB3 = libvectornav.so
TARGET_LIB4 = liboptoforce.so
TARGET_LIB5 = libNNkeras.so
#`pkg-config --libs opencv`
SOURCES1 =$(LIB_HOME)/DXLite-master/dxsystem/userinputfn.cpp \
	$(LIB_HOME)/DXLite-master/dxsystem/setup.cpp \
	$(LIB_HOME)/DXLite-master/dxsystem/dxsystem.cpp \
	$(LIB_HOME)/DXLite-master/dxsystem/dxsinglemotor.cpp \
	$(LIB_HOME)/DXLite-master/dxsystem/dxsetupmotor.cpp \
	$(LIB_HOME)/DXLite-master/dxsystem/dxmotorsystem.cpp \
	$(LIB_HOME)/DXLite-master/dxsystem/dmodeldetails.cpp \
	$(LIB_HOME)/DXLite-master/dxsystem/additionaltools.cpp \
	$(LIB_HOME)/DXLite-master/dxsystem/dxllibclass/dynamixelclass.cpp \
	$(LIB_HOME)/DXLite-master/dxsystem/dxllibclass/dxlhal.cpp

SOURCES2 = $(LIB_HOME)/Comm/source/joystick.cpp \
    $(LIB_HOME)/Comm/source/ps3joy.cpp 

SOURCES3 = $(LIB_HOME)/vectornav/vn100.cpp \
    $(LIB_HOME)/vectornav/vndevice.cpp \
    $(LIB_HOME)/vectornav/arch/linux/vncp_services.cpp 

SOURCES4 = $(LIB_HOME)/Optoforce/source/rs232.cpp \
    $(LIB_HOME)/Optoforce/source/optoforce.cpp

SOURCES5 = $(LIB_HOME)/nnTools/NeuralNet.cpp \
    $(LIB_HOME)/nnTools/keras_model.cpp

OBJS1 = $(SOURCES1:.cpp=.o)
OBJS2 = $(SOURCES2:.cpp=.o)
OBJS3 = $(SOURCES3:.cpp=.o)
OBJS4 = $(SOURCES4:.cpp=.o)
OBJS5 = $(SOURCES5:.cpp=.o)

.PHONY: all
all: ${TARGET_LIB1} ${TARGET_LIB2} ${TARGET_LIB3} ${TARGET_LIB4} ${TARGET_LIB5}

$(TARGET_LIB1): $(OBJS1)
	$(CXX) ${LIBS} -o $@ $^

$(SOURCES1:.cpp=.d):%.d:%.cpp
	$(CXX) $(CXXFLAGS) -MM $< >$@

$(TARGET_LIB2): $(OBJS2)
	$(CXX) ${LIBS} -o $@ $^

$(SOURCES2:.cpp=.d):%.d:%.cpp
	$(CXX) $(CXXFLAGS) -MM $< >$@

$(TARGET_LIB3): $(OBJS3)
	$(CXX) ${LIBS} -o $@ $^

$(SOURCES3:.cpp=.d):%.d:%.cpp
	$(CXX) $(CXXFLAGS) -MM $< >$@

$(TARGET_LIB4): $(OBJS4)
	$(CXX) ${LIBS} -o $@ $^

$(SOURCES4:.cpp=.d):%.d:%.cpp
	$(CXX) $(CXXFLAGS) -MM $< >$@

$(TARGET_LIB5): $(OBJS5)
	$(CXX) ${LIBS} -o $@ $^

$(SOURCES5:.cpp=.d):%.d:%.cpp
	$(CXX) $(CXXFLAGS) -MM $< >$@


include $(SOURCES1:.cpp=.d)
include $(SOURCES2:.cpp=.d)
include $(SOURCES3:.cpp=.d)
include $(SOURCES4:.cpp=.d)
include $(SOURCES5:.cpp=.d)

.PHONY: clean
clean:
	-${RM} ${TARGET_LIB1} ${OBJS1} $(SOURCES1:.cpp=.d)
	-${RM} ${TARGET_LIB2} ${OBJS2} $(SOURCES2:.cpp=.d)
	-${RM} ${TARGET_LIB3} ${OBJS3} $(SOURCES3:.cpp=.d)
	-${RM} ${TARGET_LIB4} ${OBJS4} $(SOURCES4:.cpp=.d)
	-${RM} ${TARGET_LIB5} ${OBJS5} $(SOURCES5:.cpp=.d)

