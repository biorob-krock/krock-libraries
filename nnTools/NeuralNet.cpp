#include "NeuralNet.hpp"
#include "keras_model.hpp"

#include <vector>
#include <string>

/* Reads file, skipps commented lines starting with "//" and stores the data into an array */
int
readFileWithLineSkipping2(ifstream& inputfile, stringstream& file){
    string line;
    file.str(std::string());
    int linenum=0;
    while (!inputfile.eof()){
        getline(inputfile,line);

        //line.erase(line.begin(), find_if(line.begin(), line.end(), not1(ptr_fun<int, int>(isspace))));
        if (line.length() == 0 || (line[0] == '#') || (line[0] == ';')){
            //continue;
        }
        else
            file << line << "\n";
            linenum++;
        }
    return linenum-1;
}

NeuralNet::NeuralNet()
{

}

NeuralNet::NeuralNet(std::string modelName, std::string configFile, int Ninput, int Noutput)
{
	init(modelName, configFile, Ninput, Noutput);
	/*
	// resize stuff
	in.Resize(Ninput);
	output.Resize(Noutput);
	scaling.resize(Ninput);
	offset.resize(Ninput);

	// load config file
	ifstream configFile_(configFile);
	stringstream stringstream_file;
	readFileWithLineSkipping(configFile_, stringstream_file);

	for(int i=0; i<Ninput; i++){
		stringstream_file >> scaling[i];
		stringstream_file >> offset[i];
	}


	// load model
	model.LoadModel(modelName);*/
}

std::vector<float> NeuralNet::predict(std::vector<float> input)
{	
	//static ofstream logfile("NNLOG.txt");
	// scale inputs
	for(uint i=0; i<in_offset.size(); i++){
		input[i]=(input[i]-in_offset[i]) / in_scaling[i];
	}
	
	/*for(int i=0; i<input.size(); i++){
		logfile << input[i] << "\t";
	}*/
	// run the network
	in.data_ = input;
	model.Apply(&in, &out);
	output=out.data_;

	/*for(int i=0; i<output.size(); i++){
		logfile << output[i] << "\t";
	}
	logfile << endl;*/
	// scale the outputs
	for(uint i=0; i<out_offset.size(); i++){
		output[i]=output[i]*out_scaling[i]+out_offset[i];
	}
	


	return output;
}

void
NeuralNet::init(std::string modelName, std::string configFile, int Ninput, int Noutput)
{

	// resize stuff
	in.Resize(Ninput);
	out.Resize(Noutput);

	output.resize(Noutput);
	in_scaling.resize(Ninput);
	in_offset.resize(Ninput);

	out_scaling.resize(Noutput);
	out_offset.resize(Noutput);

	// load config file
	ifstream configFile_(configFile);
	stringstream stringstream_file;
	readFileWithLineSkipping2(configFile_, stringstream_file);

	for(int i=0; i<Ninput; i++){
		stringstream_file >> in_offset[i];
		stringstream_file >> in_scaling[i];
	}

	for(int i=0; i<Noutput; i++){
		stringstream_file >> out_offset[i];
		stringstream_file >> out_scaling[i];
	}


	// load model
	model.LoadModel(modelName);

	
}