#include "keras_model.hpp"
#include "NeuralNet.hpp"
//#include "utils.hpp"
#include <fstream>
#include <vector>
#include <string>
#include <iostream>

/*
int main() {
    // Initialize model.
    KerasModel model;
    model.LoadModel("test.model");

    // Create a 1D Tensor on length 20 for input data.
    Tensor in(20);
    in.data_ = {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};

    // Run prediction.
    Tensor out;
    model.Apply(&in, &out);
    out.Print();
    return 0;
}

*/

using namespace std;
int main()
{
    std::vector<float> prediction(3);
    std::vector<float> target(3);
    std::vector<float> input(28);

    //ifstream inputlog("/home/odroid/PROJECTS/SingleLegController/data/inputlog.txt");
    ifstream inputlog("/home/odroid/PROJECTS/RobotController/data/test_small.txt");
    ofstream outputlog("./outputlog.txt");
    stringstream stringstream_file;
    bool is_init=false;

    cout << "reading inputs" << endl;
    readFileWithLineSkipping2(inputlog, stringstream_file);

    cout << "reading nn model" << endl;
    //NeuralNet Net("test.model", "configFile.txt", 24, 3);
    NeuralNet Net;
    Net.init("/home/odroid/PROJECTS/RobotController/config/nn/model28in.model", 
                  "/home/odroid/PROJECTS/RobotController/config/nn/config28in.txt", 28, 3);
    //NeuralNet Net("/home/odroid/PROJECTS/RobotController/config/nn/model2ndBest.model", 
    //              "/home/odroid/PROJECTS/RobotController/config/nn/configFile2ndBest.txt", 24, 3);

    for(int k=0; k<15000; k++){
        if(k%100==0){
            cout << k << endl;
        }
        for(int i=0; i<20; i++){
           stringstream_file >> input[i];
        }
        input[20]=input[12] - input[16];
        input[21]=input[13] - input[17];
        input[22]=input[14] - input[18];
        input[23]=input[15] - input[19];

        stringstream_file >> input[24];
        stringstream_file >> input[25];
        stringstream_file >> input[26];
        stringstream_file >> input[27];

        stringstream_file >> target[0];
        stringstream_file >> target[1];
        stringstream_file >> target[2];


        prediction = Net.predict(input);

        outputlog << prediction[0] << "\t" << prediction[1] << "\t" << prediction[2] << "\t";
        outputlog << target[0] << "\t" << target[1] << "\t" << target[2] << endl;

    }



    
    return 0;
}


