#ifndef NEURALNET_H
#define NEURALNET_H

#include "keras_model.hpp"
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>


using namespace std;

int readFileWithLineSkipping2(ifstream& inputfile, stringstream& file);

class NeuralNet
{
	private:
		KerasModel model;
		Tensor in;
		Tensor out;

		vector<float> output;


		vector<float> in_scaling;
		vector<float> in_offset;

		vector<float> out_scaling;
		vector<float> out_offset;





	public:
		NeuralNet();
		NeuralNet(std::string modelName, std::string configFile, int Ninput, int Noutput);
		void init(std::string modelName, std::string configFile, int Ninput, int Noutput);

		std::vector<float> predict(std::vector<float> input);
};

#endif // NEURALNET_H