#include <iostream>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <fstream>
#include <cmath>
#include "utils.hpp"
#include <unistd.h>
#include <SerialStream.h>
#include "ps3joy.hpp"
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include "optoforce.hpp"

using namespace std;
using boost::asio::ip::udp;
using namespace boost::interprocess;

using namespace LibSerial;

bool VERBOSE = 0; 		// Semi-useful printouts
bool DEBUG_PRINTOUTS = 0;	// Bread-crumbs for debugging left by Matt 
bool SKIP_UNITY = 1;		// Disabling Unity since we are not longer using this 
bool DEBUG_SKIP = 0; 		// Toggling some parts of the code to see what is holding me up 

bool isRunning(const char* name)  
{  
	char command[32];  
	sprintf(command, "pgrep %s > /dev/null", name);  
	return 0 == system(command);  
} 


int main(){
	cout << "starting" << endl;
	bool joystick_plugged;
	bool start_pressed, start_pressed_old=false;
	bool select_pressed, select_pressed_old=false;
	bool joy_l2, joy_l1, joy_r1, joy_r2;
	
	// ================= INITIALIZE JOYSTICK =====================================
	shared_memory_object::remove("ps3shm");
	PS3joy ps3joy((const char*)"ps3shm");
	joystick_plugged = ps3joy.OpenDevice((char*)"/dev/input/js0");
	cout << "Joystick initialized" << endl;

	// ================= INITIALIZE SENSORS =====================================
	SensorsStruct sensorsStruct;
	Optoforce opt3, opt4;
	opt3.init("/dev/optfor3", 2);
    opt4.init("/dev/optfor4", 3);
    double optData[6];

    shared_memory_object::remove("sensorsStruct_shm");
    managed_shared_memory ssshm(open_or_create, "sensorsStruct_shm", 1024);
    SensorsStruct *sensorsStruct_shm;
	sensorsStruct_shm = ssshm.find_or_construct<SensorsStruct>("SensorsStruct")();
	
	// ================= INITIALIZE ROBOT STATUS =====================================
	shared_memory_object::remove("rstatus2unity_shm");
	RobotStatus rStatus((const char*)"rstatus2unity_shm");
	rStatus.WriteSharedMemory(); printf("written to a shm\n");

	// ================= INITIALIZE TTL SWITCH =====================================
	SerialPort ttlSwitch("/dev/ttl_switch");
	ttlSwitch.Open(SerialPort::BAUD_57600, SerialPort::CHAR_SIZE_8, SerialPort::PARITY_NONE, 
				SerialPort::STOP_BITS_1, SerialPort::FLOW_CONTROL_HARD);
	

	// ================= INITIALIZE UDP =====================================
    boost::asio::io_service io_service;
    udp::socket socket(io_service, udp::endpoint(udp::v4(), 9010));
    boost::asio::socket_base::receive_buffer_size option(sizeof(SensorsStruct));
	socket.set_option(option);
    udp::endpoint remote_endpoint;
    boost::system::error_code error;
    boost::system::error_code ignored_error;

    // initialize buffers
    vector<RobotStatusStruct> buffer_RobotStatus;
    buffer_RobotStatus.push_back(rStatus.robotStatusStruct);

	// ================= INITIALIZE THE LOOP =====================================
	const double Td=10/1000.;
	double t=0, dt, t0, t1, t2;
	t0=get_timestamp();
	int k=0; 
	bool motorsOn=false;
	SensorsStruct jsTmp;
	// ================= THE LOOP =====================================
	while(1){
		
		dt=get_timestamp()-t0;
		t0=get_timestamp();
		t=t+dt;


		// ============================= READ JOYSTICK ===================================
		if(!joystick_plugged){
        	joystick_plugged = ps3joy.OpenDevice((char*)"/dev/input/js0");
        }
        else{
            ps3joy.GetInput();
            ps3joy.WriteSharedMemory();
        }
		if(DEBUG_PRINTOUTS){
			cout << "Joystick plugged: " << joystick_plugged << endl;
		}
        
        // ============================= READ OPTOFORCE ===================================
		if(DEBUG_PRINTOUTS){
			cout << "Updating Optoforce" << endl;
		}
		opt3.update();
		opt4.update();

    	// ============================= RECEIVE SENSORS ===================================
		if(DEBUG_PRINTOUTS){
			cout << "Receiving Sensors" << endl;
		}
		
			vector<unsigned char> input_buffer_vec(sizeof(SensorsStruct), 0);
			socket.receive_from(boost::asio::buffer(input_buffer_vec, sizeof(SensorsStruct)), remote_endpoint, 0, error);
			memcpy(&jsTmp, input_buffer_vec.data(), sizeof(SensorsStruct));
			sensorsStruct=jsTmp;

			if (error && error != boost::asio::error::message_size){
				throw boost::system::system_error(error);
			}

			// ============================= WRITE SENSORS TO SHARED MEMORY ===================================
			if(DEBUG_PRINTOUTS){
				cout << "Writing Sensors to Shared Memory" << endl;
			}
			sensorsStruct.optoforce_plugged[2]=1;
			sensorsStruct.optoforce_plugged[3]=1;

			sensorsStruct.optoforce[6] =  opt3.Fx;
			sensorsStruct.optoforce[7] =  opt3.Fy;
			sensorsStruct.optoforce[8] =  opt3.Fz;
			sensorsStruct.optoforce[9]  = opt4.Fx;
			sensorsStruct.optoforce[10] = opt4.Fy;
			sensorsStruct.optoforce[11] = opt4.Fz;

			for(int k=0; k<4; k++){
				sensorsStruct.optoforceRaw[8+k] = opt3.St[k];
				sensorsStruct.optoforceRaw[12+k] = opt4.St[k];
			}
			

			memcpy(sensorsStruct_shm, &sensorsStruct, sizeof(sensorsStruct));

			if(VERBOSE){
				cout << "optoforce \t";
				for(int i=0; i<12; i++){
					cout << sensorsStruct.optoforce[i] << "\t";
				}
				cout << "imu \t";
				for(int i=0; i<3; i++){
					cout << sensorsStruct.imu[i] << "\t";
				}	
				cout << endl;
			}
        //========= POWERMOTORS ================
		
		if(ps3joy.joyStruct.select_pressed && !ps3joy.joyStruct.select_pressed_old && !ps3joy.joyStruct.LR_pressed){
			motorsOn=!motorsOn;
			
			if(VERBOSE){
				cout << "Toggling Motor Power" << endl; 
			}
		}
		ttlSwitch.SetRts(!motorsOn);

		//========= RUN or STOP robotController PROGRAM IF START IS PRESSED ================
		if(ps3joy.joyStruct.start_pressed && !ps3joy.joyStruct.start_pressed_old  && !ps3joy.joyStruct.LR_pressed){
			if(isRunning("robot_main")) {
				printf("MAIN IS RUNNING. STOPING IT NOW!\n");
				 //A process having name robot_main is running.
			}
			else{
				printf("Options button pressed. Starting robot_main pointing to ROS \n");
				system("cd /home/biorob/controllerkrock2; LD_LIBRARY_PATH=/opt/ros/melodic/lib ROS_MASTER_URI=http://nccr-local-linux:11311 ./robot_main & ");
			  //A process having name robot_main is NOT running.

			}
		}
		
		//========= RUN or STOP robotController PROGRAM IF START IS PRESSED ================
		


		if(!SKIP_UNITY){
			//========= RUN or STOP UnitySendStatus PROGRAM IF START IS PRESSED ================
			if(ps3joy.joyStruct.select_pressed && !ps3joy.joyStruct.select_pressed_old && ps3joy.joyStruct.LR_pressed){
				
				system("ssh root@192.168.1.190 'pkill camera_main; pkill unitySendStatus; /home/odroid/Libraries/Comm/UnitySendStatus/unitySendStatus &'");
				if(isRunning("robot_main")) {
					printf("MAIN IS RUNNING. STOPING IT NOW!\n");
					//A process having name robot_main is running.
				}
				else{
					printf("MAIN IS NOT RUNNING. STARTING IT NOW!\n");
					system("cd /home/odroid/PROJECTS/RobotController; ./robot_main & ");
				//A process having name robot_main is NOT running.

				}
			}
		}

		//========= Shutdown ODROID if robot is not running & L1+R1 are pressed ================
		if(!motorsOn && !isRunning("robot_main") ){ 
			if(ps3joy.joyStruct.buttons[4] && ps3joy.joyStruct.buttons[5]){
				printf("\nL1+R1 pressed, shutting down now.\n");
				system("sudo shutdown now");
			}
		}
		


        // ============================= SEND ROBOT STATUS===================================
        // update robot status
		if(DEBUG_PRINTOUTS){
			cout << "Sending Robot Status" << endl; 
		}
        buffer_RobotStatus.pop_back();
        rStatus.ReadSharedMemory();
        // update motor status
        rStatus.robotStatusStruct.motorsOn=motorsOn;

        buffer_RobotStatus.push_back(rStatus.robotStatusStruct);

        // send udp
        remote_endpoint.port(9010);
        socket.send_to(boost::asio::buffer(buffer_RobotStatus), remote_endpoint, 0, ignored_error);
        //cout << "IP: " << remote_endpoint.address() << "\t" << "port: " << remote_endpoint.port() <<"\t" << buffer_RobotStatus.size()<<endl;


        //=========================== TIME STEP ===============================
		t1=get_timestamp();
		cout.flush();
		if(Td>(t1-t0)){
		    //usleep((Td- (t1-t0)) );
		    //boost::this_thread::sleep(boost::posix_time::milliseconds(100));
		    sleepBoost(Td- (t1-t0));
		}
		t2=get_timestamp();
		//printf("%4.5f ms \t\t %4.5f ms \n", (t1 - t0) * 1000, (t2 - t0) * 1000);
		if(DEBUG_PRINTOUTS){
			cout << "End of Control Loop" << endl; 
		}
    }

	return 0;
}


