#ifndef XBEE_HPP
#define XBEE_HPP

#include <fstream>
#include <vector>
#include <string.h>
#include <SerialStream.h>
#include <SerialPort.h>
#include "struct_defs.hpp"

using namespace std;
using namespace LibSerial;

#define XBEE_MASTER true
#define XBEE_SLAVE false


/* XBEE Class */
class Xbee
{

	
	public:
		//SerialStream xbeeStream;
		SerialPort xbeePort;
        bool token;
        bool role;
        int failed_sending, isopen;
        int nSlaves;
        //unsigned char identifier[10];
		
		Xbee(string device, bool role_in);
		bool sendJoy(struct JoyStruct message);
		bool sendJoy(struct JoyStructPack message);
        bool sendRStatus(struct RobotStatus rstatus1);
		void getJoy(struct JoyStruct *joy1, short int joyNum);
        struct RobotStatus getRStatus();
		void closePort();


};





#endif


