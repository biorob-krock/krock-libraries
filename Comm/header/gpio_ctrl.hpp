#ifndef GPIO_CTRL_HPP
#define GPIO_CTRL_HPP

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
 
#include <unistd.h>
#include <string.h>
#include <time.h>
 
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <wiringSerial.h>
#include <lcd.h> 


using namespace std;



int gpio_system_init(const int *ports, int size);

void set_gpio(int port, bool value);

void toggle_gpio(int port);











#endif