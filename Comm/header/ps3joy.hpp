#ifndef PS3JOY_HPP
#define PS3JOY_HPP

#include "joystick.h"
#include <stdlib.h>
#include "struct_defs.hpp"
#include <fstream>
#include <vector>
#include <string.h>
#include <iostream>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>

using namespace std;
using namespace boost::interprocess;



/* PS3JOY Class */
class PS3joy
{
	public:	
		// functions
		PS3joy(const char *sharedMemoryName);
		bool IsReady();
		JoyStruct GetInput();
		void PrintInput();
		void SetInput(JoyStruct joyData);
		bool OpenDevice(char * name);
		void OpenSharedMemory(const char * name);
		void CreateSharedMemory(const char *sharedMemoryName, bool destruct);
		void WriteSharedMemory();
		void ReadSharedMemory();


		// variables
		JoyStruct joyStruct;
	private:
		// functions
		managed_shared_memory shm;


		// variables
		

		Joystick js;
		js_event event;
		
		JoyStruct *joyStruct_shm;

		float joy_x1, joy_x2, joy_y1, joy_y2, joy_x3, joy_y3;
	    int joy_l1, joy_l2, joy_l3, joy_r1, joy_r2, joy_r3, joy_sel, joy_start, joy_bD, joy_bL, joy_bR, joy_bU;
	    //double joy_lsr, joy_rsr;
	    //double joy_lsphi, joy_rsphi;

};



/* ROBOT STATUS Class */
class RobotStatus
{
	public:	
		// functions
		RobotStatus(const char *sharedMemoryName);
		void CreateSharedMemory(const char *sharedMemoryName, bool destruct);
		void WriteSharedMemory();
		void ReadSharedMemory();

		// variables
		RobotStatusStruct robotStatusStruct;
		managed_shared_memory shm;
	
		RobotStatusStruct *robotStatusStruct_shm;
	private:
		// variables
		

};













#endif

