#ifndef STRUCT_DEFS_HPP
#define STRUCT_DEFS_HPP


#pragma pack(push, 1)  // exact fit - no padding
struct JoyStruct
{
	float axes[6];
	int buttons[16];
	bool start_pressed, start_pressed_old, select_pressed, select_pressed_old, LR_pressed;
};

struct RobotStatusStruct{
	float motor_positions[27];
	float motor_torques[27];
	float imu[3];
	float forces[12];
	bool motorsOn;
};

struct SensorsStruct{
	bool optoforce_plugged[4]={0};
	double optoforce[12]={0};
	unsigned short int optoforceRaw[16]={0};
	bool imu_plugged=0;
	double imu[3]={0};
};

#pragma pack(pop) //back to whatever the previous packing mode was







#endif