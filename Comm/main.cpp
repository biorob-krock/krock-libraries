#include <iostream>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <fstream>
#include <cmath>
#include "xbee.hpp"
#include "utils.hpp"
#include <unistd.h>
#include <SerialStream.h>
#include "ps3joy.hpp"
#include "gpio_ctrl.hpp"


using namespace std;
using namespace LibSerial;

#define XBEE_MASTER true
#define XBEE_SLAVE false

 bool isRunning(const char* name)  
 {  
   char command[32];  
   sprintf(command, "pgrep %s > /dev/null", name);  
   return 0 == system(command);  
 } 


int main(){
	bool joystick_plugged;
	bool start_pressed, start_pressed_old=false;
	bool select_pressed, select_pressed_old=false;
	bool joy_l2, joy_l1, joy_r1, joy_r2;
	bool motorsOn=false;

	PS3joy ps3joy((const char*)"ps3shm");
	joystick_plugged = ps3joy.OpenDevice((char*)"/dev/input/js0");
	ps3joy.WriteSharedMemory(); printf("written to a shm\n");

	struct JoyStruct joy1;
	for(int i=0; i<6; i++){
		joy1.axes[i]=0;
	}
	for(int i=0; i<16; i++){
		joy1.buttons[i]=0;
	}

	//struct RobotStatus rstatus1;
	struct RobotStatusStruct rstatus1;
	

	Xbee xbee1("/dev/xbee_uart", XBEE_SLAVE);

	//Initialize shared memory for a robot status object
	managed_shared_memory shm(open_or_create, "rstatus_shm", 1024);
	RobotStatus *rstatus_object_shm=shm.find_or_construct<RobotStatus>("RobotStatus")();

	// initialize GPIO
	const int ports[1]={24};
	printf("%d\n", gpio_system_init(ports, 1));


	// init times
	const double Td=20/1000.;
	double t=0, dt, t0, t1, t2;
	t0=get_timestamp();
	bool message_sent=false;
	while(1){

		dt=get_timestamp()-t0;
		t0=get_timestamp();
		t=t+dt;
/*
		for(int i=0; i<30; i++){
			rstatus1.motor_positions[i]=i;
			if(i>15){
				rstatus1.motor_positions[i]=rand()*100;
			}
		}*/
		//=========================== Send message ===============================
		memcpy(&rstatus1, rstatus_object_shm, sizeof(RobotStatus));
		rstatus1.motor_positions[0]=1;
		rstatus1.motor_positions[1]=isRunning("robot_main");
		rstatus1.motor_positions[3]=motorsOn;
		if(xbee1.isopen){
			message_sent=xbee1.sendRStatus(rstatus1);
		}
		
		
		
		//=========================== Receive message from XBEE ============================
		if(!joystick_plugged){
			joystick_plugged = ps3joy.OpenDevice((char*)"/dev/input/js0");	
			if(!message_sent){
				try	
				{
					if(xbee1.isopen){
						xbee1.getJoy(&joy1, 2);
						ps3joy.SetInput(joy1);

						ps3joy.WriteSharedMemory();
					}
					

					
					//cout<<"success"<<endl;
				}
				catch (const runtime_error& error)
				{
				   cout<<"read timeout"<<endl;
				}
			}
		}
		else{
			ps3joy.GetInput();
			ps3joy.WriteSharedMemory();
		}

		//ps3joy.PrintInput();

		//========= RUN robot_demo PROGRAM IF START IS PRESSED ================
		start_pressed=((ps3joy.joyStruct.buttons[0] & (1<<3))!=0);   // ps3joy.joyStruct.buttons[3];
		if(start_pressed && !start_pressed_old){
			if(isRunning("robot_main")) {
				printf("MAIN IS RUNNING. STOPING IT NOW!\n");
				 //A process having name robot_main is running.
			}
			else{
				printf("MAIN IS NOT RUNNING. STARTING IT NOW!\n");
				system("cd /home/odroid/PROJECTS/Robot_controller; ./robot_main &");
			  //A process having name robot_main is NOT running.

			}
		}

		

		


		//========= POWER MOTORS ================
		select_pressed=((ps3joy.joyStruct.buttons[0] & (1))!=0);   // ps3joy.joyStruct.buttons[0];
		if(select_pressed && !select_pressed_old){
			motorsOn=!motorsOn;
			//toggle_gpio(ports[0]);
		}
		set_gpio(ports[0], motorsOn);
		


		//========= KILL PROGRAM  ================
		if(start_pressed && !start_pressed_old && select_pressed){
			system("pkill robot_main");
		}






		//========= TURN OFF ODROID  ================	
		joy_l2=((ps3joy.joyStruct.buttons[1] & (1 << 0))!=0);
        joy_r2=((ps3joy.joyStruct.buttons[1] & (1 << 1))!=0);
        joy_l1=((ps3joy.joyStruct.buttons[1] & (1 << 2))!=0);
        joy_r1=((ps3joy.joyStruct.buttons[1] & (1 << 3))!=0);


        if(joy_l2 && joy_l1 && joy_r1 && joy_r2 && !select_pressed_old && select_pressed){
			system("shutdown -h now");
		}

		select_pressed_old=((ps3joy.joyStruct.buttons[0] & (1))!=0);   // ps3joy.joyStruct.buttons[0];
		start_pressed_old=((ps3joy.joyStruct.buttons[0] & (1<<3))!=0);   // ps3joy.joyStruct.buttons[3];



		//=========================== TIME STEP ===============================
		t1=get_timestamp();
		cout.flush();
		if(Td>(t1-t0)){
		    //usleep((Td- (t1-t0)) );
		    //boost::this_thread::sleep(boost::posix_time::milliseconds(100));
		    sleepBoost(Td- (t1-t0));
		}
		t2=get_timestamp();
		//printf("%4.5f ms \t\t %4.5f ms \n", (t1 - t0) * 1000, (t2 - t0) * 1000);
		
	}








    xbee1.closePort();


	cout<<"Port closed"<<endl;
	return 0;
}































