#include <iostream>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <fstream>
#include <cmath>
#include "utils.hpp"
#include <unistd.h>
#include <SerialStream.h>
#include "ps3joy.hpp"
#include "gpio_ctrl.hpp"
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include "struct_defs.hpp"

using namespace std;
using boost::asio::ip::udp;

bool isRunning(const char* name)  
{  
	char command[32];  
	sprintf(command, "pgrep %s > /dev/null", name);  
	return 0 == system(command);  
} 


int main(){
	bool joystick_plugged;
	bool start_pressed, start_pressed_old=false;
	bool select_pressed, select_pressed_old=false;
	bool joy_l2, joy_l1, joy_r1, joy_r2;
	bool motorsOn=false;

	// ================= INITIALIZE JOYSTICK =====================================
	shared_memory_object::remove("ps3shm");
	PS3joy ps3joy((const char*)"ps3shm");
	joystick_plugged = ps3joy.OpenDevice((char*)"/dev/input/js0");
	cout << "joystick initialized" << endl;
	// ================= INITIALIZE ROBOT STATUS =====================================
	shared_memory_object::remove("rstatus2unity_shm");
	RobotStatus rStatus((const char*)"rstatus2unity_shm");
	rStatus.WriteSharedMemory(); printf("written to a shm\n");

	// ================= INITIALIZE UDP XU4=====================================
    boost::asio::io_service io_service;
    udp::endpoint remote_endpoint(udp::v4(), 9010);
    remote_endpoint.address(boost::asio::ip::address_v4::from_string("192.168.1.191"));
    udp::socket socket(io_service, udp::endpoint(udp::v4(), 9010));
    boost::system::error_code error, ignored_error;
    

    // initialize buffers
    vector<JoyStruct> buffer_JoyStruct;
    buffer_JoyStruct.push_back(ps3joy.joyStruct);

	// init times
	const double Td=20/1000.;
	double t=0, dt, t0, t1, t2;
	t0=get_timestamp();
	cout << "starting loop" << endl;
	int k=0; 
	RobotStatusStruct rsTmp;
	while(1){
		cout << k++ << endl;

		dt=get_timestamp()-t0;
		t0=get_timestamp();
		t=t+dt;


		// UDP
        try
        {	
        	// ============================= RECEIVE ===================================
        	cout << socket.available() << endl;
        	if(socket.available()){
        		vector<unsigned char> input_buffer_vec(sizeof(RobotStatus), 0);
        		socket.receive(boost::asio::buffer(input_buffer_vec, sizeof(RobotStatus)), 0, error);

        		//robotStatusStruct RobotStatus::*ptr=&RobotStatus::robotStatusStruct;  

        		memcpy(&rsTmp, input_buffer_vec.data(), sizeof(RobotStatus));
        		rStatus.robotStatusStruct=rsTmp;
        		//rStatus.ReadSharedMemory();
	        	cout <<"motors on: "<<rStatus.robotStatusStruct.motorsOn<< endl;
	        	rStatus.WriteSharedMemory();

        	}
            
            if (error && error != boost::asio::error::message_size){
                throw boost::system::system_error(error);
            }

            // ============================= SEND ===================================
            // update joystick readings
            buffer_JoyStruct.pop_back();
            ps3joy.GetInput();
            buffer_JoyStruct.push_back(ps3joy.joyStruct);
      
            
            socket.send_to(boost::asio::buffer(buffer_JoyStruct), remote_endpoint, 0, ignored_error);

        }
        catch (std::exception& e)
        {
            std::cerr << e.what() << std::endl;
        }

    
/*
		//=========================== Send message ===============================
		memcpy(&rstatus1, rstatus_object_shm, sizeof(RobotStatus));
		rstatus1.motor_positions[0]=1;
		rstatus1.motor_positions[1]=isRunning("robot_main");
		rstatus1.motor_positions[3]=motorsOn;
		if(xbee1.isopen){
			message_sent=xbee1.sendRStatus(rstatus1);
		}
		
		
		
		//=========================== Receive message from XBEE ============================
		if(!joystick_plugged){
			joystick_plugged = ps3joy.OpenDevice((char*)"/dev/input/js0");	
			if(!message_sent){
				try	
				{
					if(xbee1.isopen){
						xbee1.getJoy(&joy1, 2);
						ps3joy.SetInput(joy1);

						ps3joy.WriteSharedMemory();
					}
					

					
					//cout<<"success"<<endl;
				}
				catch (const runtime_error& error)
				{
				   cout<<"read timeout"<<endl;
				}
			}
		}
		else{
			ps3joy.GetInput();
			ps3joy.WriteSharedMemory();
		}

		//ps3joy.PrintInput();

		//========= RUN robot_demo PROGRAM IF START IS PRESSED ================
		start_pressed=((ps3joy.joyStruct.buttons[0] & (1<<3))!=0);   // ps3joy.joyStruct.buttons[3];
		if(start_pressed && !start_pressed_old){
			if(isRunning("robot_main")) {
				printf("MAIN IS RUNNING. STOPING IT NOW!\n");
				 //A process having name robot_main is running.
			}
			else{
				printf("MAIN IS NOT RUNNING. STARTING IT NOW!\n");
				system("cd /home/odroid/PROJECTS/Robot_controller; ./robot_main &");
			  //A process having name robot_main is NOT running.

			}
		}

		

		


		//========= POWER MOTORS ================
		select_pressed=((ps3joy.joyStruct.buttons[0] & (1))!=0);   // ps3joy.joyStruct.buttons[0];
		if(select_pressed && !select_pressed_old){
			motorsOn=!motorsOn;
			//toggle_gpio(ports[0]);
		}
		set_gpio(ports[0], motorsOn);
		


		//========= KILL PROGRAM  ================
		if(start_pressed && !start_pressed_old && select_pressed){
			system("pkill robot_main");
		}






		//========= TURN OFF ODROID  ================	
		joy_l2=((ps3joy.joyStruct.buttons[1] & (1 << 0))!=0);
        joy_r2=((ps3joy.joyStruct.buttons[1] & (1 << 1))!=0);
        joy_l1=((ps3joy.joyStruct.buttons[1] & (1 << 2))!=0);
        joy_r1=((ps3joy.joyStruct.buttons[1] & (1 << 3))!=0);


        if(joy_l2 && joy_l1 && joy_r1 && joy_r2 && !select_pressed_old && select_pressed){
			system("shutdown -h now");
		}

		select_pressed_old=((ps3joy.joyStruct.buttons[0] & (1))!=0);   // ps3joy.joyStruct.buttons[0];
		start_pressed_old=((ps3joy.joyStruct.buttons[0] & (1<<3))!=0);   // ps3joy.joyStruct.buttons[3];


*/
		//=========================== TIME STEP ===============================
		t1=get_timestamp();
		cout.flush();
		if(Td>(t1-t0)){
		    //usleep((Td- (t1-t0)) );
		    //boost::this_thread::sleep(boost::posix_time::milliseconds(100));
		    sleepBoost(Td- (t1-t0));
		}
		t2=get_timestamp();
		//printf("%4.5f ms \t\t %4.5f ms \n", (t1 - t0) * 1000, (t2 - t0) * 1000);
		
	}








	return 0;
}































