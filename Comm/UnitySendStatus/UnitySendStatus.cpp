#include <iostream>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <fstream>
#include <cmath>
#include "utils.hpp"
#include <unistd.h>
#include <SerialStream.h>
#include "ps3joy.hpp"
#include "gpio_ctrl.hpp"
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include "struct_defs.hpp"

using namespace std;
using boost::asio::ip::udp;

bool isRunning(const char* name)  
{  
	char command[32];  
	sprintf(command, "pgrep %s > /dev/null", name);  
	return 0 == system(command);  
} 


int main(){
	// ================= INITIALIZE ROBOT STATUS =====================================

	RobotStatus rStatus((const char*)"rstatus2unity_shm");
	//rStatus.CreateSharedMemory((const char*)"rstatus2unity_shm", true);
	//rStatus.WriteSharedMemory(); printf("written to a shm\n");

	// ================= INITIALIZE UDP UNITY=====================================
    boost::asio::io_service io_service;
    udp::socket socket(io_service, udp::endpoint(udp::v4(), 9015));
    udp::endpoint remote_endpoint;
    boost::system::error_code error;
    boost::system::error_code ignored_error;
    
    // initialize buffers
    vector<RobotStatusStruct> buffer_RobotStatus;
    buffer_RobotStatus.push_back(rStatus.robotStatusStruct);

	// init times
	cout << "starting loop" << endl;
	int k=0; 
	while(1){
		cout << k++ << endl;


		// UDP
    	// ============================= RECEIVE REQUEST FROM UNITY===================================
    	vector<unsigned char> input_buffer_vec(1024, 0);
        socket.receive_from(boost::asio::buffer(input_buffer_vec), remote_endpoint, 0, error);
        if (error && error != boost::asio::error::message_size){
            throw boost::system::system_error(error);
        }

        // ============================= SEND ROBOT STATUS===================================
        // update robot status
        buffer_RobotStatus.pop_back();
        rStatus.ReadSharedMemory();
        buffer_RobotStatus.push_back(rStatus.robotStatusStruct);

        // send udp
        remote_endpoint.port(9015);
        socket.send_to(boost::asio::buffer(buffer_RobotStatus), remote_endpoint, 0, ignored_error);
        //cout << "IP: " << remote_endpoint.address() << "\t" << "port: " << remote_endpoint.port() <<"\t motorsOn:" << rStatus.robotStatusStruct.motorsOn<<endl;
	   for(int i=0; i<10; i++){
            cout << rStatus.robotStatusStruct.motor_positions[i] << "\t";
       }
       cout << endl;
	}



	return 0;
}































