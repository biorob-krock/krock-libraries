 

#include "gpio_ctrl.hpp"


int gpio_system_init(const int *ports, int size)
{
    int i;
 	printf("wiringPiSetup\n");
 	wiringPiSetupGpio();
 	printf("setting ports\n");
    // GPIO Init(LED Port ALL Output)
    for(i = 0; i < size; i++)    pinMode (ports[i], OUTPUT);
 
    return  0;
 }



void set_gpio(int port, bool value)
{
	digitalWrite (port, value);
}

void toggle_gpio(int port)
{
	static bool value=true;
	value=!value;
	digitalWrite (port, value);
}