#ifndef SETUP_H
#define SETUP_H

#include "dxsetupmotor.h"
#include "dxmotorsystem.h"
#include "dxsinglemotor.h"

void dxsystemUserDefaultConfig(DXMotorSystem& d);

int dxlSetupSequence(const char *portName);

DXMotorSystem* dxsystemSetup(const int deviceIndex, const char *portName);
DXSingleMotor* dxsinglemotorSetup(const int deviceIndex, const char *portName);
vector<DXMotorSystem*> multidxsystemSetup(const int numPorts, const char *portName);
vector<DXMotorSystem*> multidxsystemSetup(vector<int> portNum, const char *portName);

#endif // SETUP_H
