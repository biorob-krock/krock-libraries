#include <iostream>

#include "dxsystem/setup.h"
#include "dxsystem/additionaltools.h"
using namespace std;

#define IS_RADIAN true
#define IS_DEGREE false
#define ANGLE_TYPE IS_DEGREE

int main()
{
    cout << "Hello World!" << endl;
    //cout << "Press 'Y' to continue:";
    //while(!getYorN());

    // Start of Initializing DXMotorSystem
    DXMotorSystem d = *dxsystemSetup(1, "/dev/usb2rs485");
    int numMotors = d.getNumMotors();
    cout << numMotors << " motors detected." << endl;

    

    while(true)
    {
        while(true){
        vector<int> position = d.getAllPosition();
        for (int i = 0; i < numMotors; i++)
        {
            cout << "Motor #" << i << ":" << endl;
            cout << "    Model:" << d.getModel(i) << endl;
            cout << "    Position:" << position[i] << endl;
            cout << "    ID:" << d.readByte(i,3) << endl;
            cout << "    Baud:" << d.readByte(i,4) << endl;
            cout << "    Delay:" << d.readByte(i,5) << endl;
            cout << "    P gain:" << d.readByte(i,28) << endl;

            //delay(100);
            //ID=d.readByte(i, 3);
            //
            //cout << "after update ID: " << ID << endl;
            //delay(100);
            //d.setByte(i, 47, 0);
            //delay(100);
            

        }
    }

        cout << "Continue?: ";
        if(!getYorN())
            break;

        cout << "New angles for motors..." << endl;
        vector<int> goalPosition(d.getNumMotors(),-1);
        int angle;
        for (int i = 17; i < numMotors; i++)
        {
            cout << "    New Angle (degrees) for Motor #" << i << ":";
            angle = getInt();
            goalPosition[i] = angle2Position(angle, d.getModel(i), ANGLE_TYPE);
        }
        //d.setAllPosition(goalPosition);
        d.setPosition(17, goalPosition[17]);
    }

    return 0;
}

