
#include "ps3joy.hpp"


/* PS3joy constructor*/
PS3joy :: PS3joy(const char *sharedMemoryName) : shm(open_or_create, sharedMemoryName, 1024)
{
  joyStruct.header[0]=HEADER_BYTE_0;
  joyStruct.header[1]=HEADER_BYTE_1;

  for(int i=0; i<2; i++){
    joyStruct.buttons[i]=0;
  }
  for(int i=0; i<4; i++){
    joyStruct.axes[i]=0;
  }


  joyStruct_shm = shm.find_or_construct<JoyStruct>("JoyStruct")();



}

/* Opens a joystick device */
bool PS3joy :: OpenDevice(char * device) 
{
  return js.load(device);
}

/* Check if joystick is ready */
bool PS3joy :: IsReady()
{
  return js.is_ready();
}

/* Check if joystick is ready */
JoyStruct PS3joy :: GetInput()
{
  js.update();

  joyStruct.buttons[0]=0;
  for(int i=0; i<8; i++){
      if(js.buttons[i])
          joyStruct.buttons[0] |= 1 << i;        
  }
  joyStruct.buttons[1]=0;
  for(int i=8; i<16; i++){
      if(js.buttons[i])
          joyStruct.buttons[1] |= 1 << (i-8);       
  }

  for(int i=0; i<4; i++){
    joyStruct.axes[i]=(signed char)(js.axes[i]*127);
  }

  return joyStruct;
}

/* Prints all the buttons and axes */
void PS3joy :: PrintInput()
{

  for(int i=0; i<4; i++){
    printf("A%d: %d \t", i , joyStruct.axes[i]);
  }
  cout << endl;
  for(int i=0; i<8; i++){
    printf("B%d: %d \t", i , (joyStruct.buttons[0] & (1<<i))!=0);  
  }
  for(int i=8; i<16; i++){
    printf("B%d: %d \t", i , (joyStruct.buttons[1] & (1<<(i-8)))!=0);
  }
  printf("\n\n");
}

/* Manually sets the input */
void PS3joy :: SetInput(JoyStruct joyData)
{
  joyStruct=joyData;
}

/* Write to a shared memory */
void PS3joy :: WriteSharedMemory()
{
  memcpy(joyStruct_shm, &joyStruct, sizeof(JoyStruct));
}

/* Write to a shared memory */
void PS3joy :: ReadSharedMemory()
{
  memcpy(&joyStruct, joyStruct_shm, sizeof(JoyStruct));
}























