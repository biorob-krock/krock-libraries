#include "xbee.hpp"
#include "struct_defs.hpp"
#include <iostream>
#include <cmath>
#include <ctime>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include <stdlib.h>
#include <algorithm>    // std::rotate

// multithreading
//#include <thread>
//#include <mutex>

/*
mutex mtx_joy;
void poolJoystick(Xbee xbee, struct JoyStruct joy){
	char input_buffer[1000];
	xbee.xbeeStream.read(input_buffer, sizeof(joy));

	mtx_joy.lock();
	memcpy(&joy, input_buffer, sizeof(joy));
	mtx_joy.unlock();
}
*/
#define USED_BAUD_RATE BAUD_57600
//#define USED_BAUD_RATE BAUD_115200
#define READ_TIMEOUT_MS 500
#define GENERATE_TOKEN_LIMIT 5
#define ASSIGNED_RECEIVE_ID ID_RED

/* Xbee constructor */
Xbee :: 
Xbee(string device, bool role_in)  : xbeePort(device)
{
	//xbeeStream.Open(device) ;
	role=role_in;
	failed_sending=0;
	try	
	{
		xbeePort.Open(SerialPort::USED_BAUD_RATE, SerialPort::CHAR_SIZE_8, SerialPort::PARITY_NONE, 
			SerialPort::STOP_BITS_1, SerialPort::FLOW_CONTROL_NONE);
	}
	catch (const runtime_error& error)
	{
		cout<<"read timeout"<<endl;
	}
	isopen=xbeePort.IsOpen();
	if(role==XBEE_MASTER){
		token=true;
	}
	else if(role==XBEE_SLAVE){
		token=false;
	}


}

/* Send joystick data */
bool Xbee ::
sendJoy(struct JoyStruct message)
{ 
	//============================ SENDING INITIALIZATION ============================
	if(failed_sending>=GENERATE_TOKEN_LIMIT && role==XBEE_MASTER){
		token=true;
	}
	if(!token){
		failed_sending++;
		return false;
	}
	token=false;
	failed_sending=0;


	//================================================================================
	char b_data[sizeof(struct JoyStruct)];
  	memcpy(b_data, (char *) &message, sizeof(struct JoyStruct));
  	xbeePort.Write(string(b_data, sizeof(struct JoyStruct)));
	
	return true;
}

/* Send joystick data */
bool Xbee ::
sendJoy(struct JoyStructPack message)
{   
	//============================ SENDING INITIALIZATION ============================
	if(failed_sending>=GENERATE_TOKEN_LIMIT && role==XBEE_MASTER){
		token=true;
	}
	if(!token){
		failed_sending++;
		return false;
	}
	token=false;
	failed_sending=0;

	//================================================================================
	char b_data[sizeof(struct JoyStructPack)];
  	memcpy(b_data, (char *) &message, sizeof(struct JoyStructPack)); //cout << sizeof(struct JoyStructPack) << endl;
  	xbeePort.Write(string(b_data, sizeof(struct JoyStructPack)));
		
	return true;
}

/* Read joystick data */
void Xbee ::
getJoy(struct JoyStruct *joy1, short int joyNum)
{	
	static int header_position;
	static struct JoyStructPack joy_pack;

	static vector<unsigned char> input_buffer_vec(sizeof(*joy1), 0);
	static vector<unsigned char> input_buffer_vec_pack(sizeof(joy_pack), 0);

	//==================================== 1 JOYSTICK =============================================
	if(joyNum==1){
		xbeePort.Read(input_buffer_vec, sizeof(*joy1), READ_TIMEOUT_MS);

		// check alignment
		for(unsigned short int i=0; i<input_buffer_vec.size()-1; i++){
			if(input_buffer_vec[i]==HEADER_BYTE_0 && input_buffer_vec[i+1]==ASSIGNED_RECEIVE_ID){
				header_position=i;
				break;
			}
		}
		if(input_buffer_vec[input_buffer_vec.size()-1]==HEADER_BYTE_0 && input_buffer_vec[0]==ASSIGNED_RECEIVE_ID){
			header_position=input_buffer_vec.size()-1;
		}

		// rotate to align
		rotate(input_buffer_vec.begin(), input_buffer_vec.begin()+header_position, input_buffer_vec.end());

		//unsigned char * input_buffer = &input_buffer_vec[0];
		unsigned char * input_buffer = input_buffer_vec.data();

		memcpy(joy1, input_buffer, sizeof(*joy1));
	}
	//==================================== 2 JOYSTICKS =============================================
	else{ 
		xbeePort.Read(input_buffer_vec_pack, sizeof(joy_pack), READ_TIMEOUT_MS);

		// check alignment
		for(unsigned short int i=0; i<input_buffer_vec_pack.size()-1; i++){
			if(input_buffer_vec_pack[i]==PACK_HEADER_BYTE_0 && input_buffer_vec_pack[i+1]==PACK_HEADER_BYTE_1){
				header_position=i;
				break;
			}
		}
		if(input_buffer_vec[input_buffer_vec_pack.size()-1]==PACK_HEADER_BYTE_0 && input_buffer_vec[0]==PACK_HEADER_BYTE_1){
			header_position=input_buffer_vec_pack.size()-1;
		}

		// rotate to align
		rotate(input_buffer_vec_pack.begin(), input_buffer_vec_pack.begin()+header_position, input_buffer_vec_pack.end());

		//unsigned char * input_buffer = &input_buffer_vec[0];
		unsigned char * input_buffer = input_buffer_vec_pack.data();

		memcpy(&joy_pack, input_buffer, sizeof(joy_pack));

		if(joy_pack.joy[0].header[1]==ASSIGNED_RECEIVE_ID){
			*joy1=joy_pack.joy[0];
		}
		//else if(joy_pack.joy[1].header[1]==ASSIGNED_RECEIVE_ID){
		else{
			*joy1=joy_pack.joy[1];
		}
	}
	

	if(joy1->token==TOKEN_BYTE){
		token=true;
	}
	else{
		token=false;
	}
	
}


/* Close port */
void Xbee ::
closePort()
{
	xbeePort.Close();
}


/* Send robot status data */
bool Xbee ::
sendRStatus(struct RobotStatus message)
{

	//============================ SENDING INITIALIZATION ============================
	if(failed_sending>=GENERATE_TOKEN_LIMIT && role==XBEE_MASTER){
		token=true;
	}
	if(!token){
		failed_sending++;
		return false;
	}
	token=false;
	failed_sending=0;
	message.token=TOKEN_BYTE;
	message.header[0]=HEADER_BYTE_0;
	message.header[1]=HEADER_BYTE_1;
	//================================================================================

	char b_data[sizeof(struct RobotStatus)];
  	memcpy(b_data, (char *) &message, sizeof(struct RobotStatus));
  	xbeePort.Write(string(b_data, sizeof(struct RobotStatus)));
		cout<<sizeof(struct RobotStatus)<<endl;
	return true;
}

/* Read robot status data */
struct RobotStatus Xbee ::
getRStatus()
{	
	int header_position;
	struct RobotStatus rstatus1;
	static vector<unsigned char> input_buffer_vec(sizeof(struct RobotStatus), 0);
	//xbeeStream.read(input_buffer, sizeof(joy1));
	xbeePort.Read(input_buffer_vec, sizeof(struct RobotStatus), READ_TIMEOUT_MS);

	// check alignment
	//header_position=0;
	for(unsigned short int i=0; i<input_buffer_vec.size()-1; i++){
		if(input_buffer_vec[i]==HEADER_BYTE_0 && input_buffer_vec[i+1]==HEADER_BYTE_1){
			header_position=i;
			break;
		}
	}
	if(input_buffer_vec[input_buffer_vec.size()-1]==HEADER_BYTE_0 && input_buffer_vec[0]==HEADER_BYTE_1){
		header_position=input_buffer_vec.size()-1;
	}

	// rotate to align
	//printf("header_position: %d\n", header_position);
	rotate(input_buffer_vec.begin(), input_buffer_vec.begin()+header_position, input_buffer_vec.end());



	unsigned char * input_buffer = input_buffer_vec.data();

	memcpy(&rstatus1, input_buffer, sizeof(struct RobotStatus));

	/*
	for(int i=0; i<16; i++){
		printf("%d \t", joy1.buttons[i]);
	}
	printf("\n");*/

	if(rstatus1.token==TOKEN_BYTE){
		token=true;
	}
	else{
		token=false;
	}
	return rstatus1;
}





































