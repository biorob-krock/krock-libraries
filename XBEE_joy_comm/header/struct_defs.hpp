#ifndef STRUCT_DEFS_HPP
#define STRUCT_DEFS_HPP

#define HEADER_BYTE_0 157
#define HEADER_BYTE_1 133
#define PACK_HEADER_BYTE_0 254
#define PACK_HEADER_BYTE_1 255
#define TOKEN_BYTE 27
#define ID_BLUE 124
#define ID_RED 125

#pragma pack(push, 1)  // exact fit - no padding
struct JoyStruct
{
	unsigned char header[2];
	signed char axes[4];
	char buttons[2];
	unsigned char token; 
};

struct RobotStatus{
	unsigned char header[2];
	signed char motor_positions[30];
	unsigned char token; 
};


struct JoyStructPack{
	unsigned char header[2];
	struct JoyStruct joy[2];
};
#pragma pack(pop) //back to whatever the previous packing mode was







#endif